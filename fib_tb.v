`timescale 1ns / 1ps

module fib_tb;

	// Inputs
	reg clk;
	reg rst_n;

	// Outputs
	wire [7:0] data;

	// Instantiate the Unit Under Test (UUT)
	fibonacci_lfsr_5bit uut (
		.clk(clk), 
		.rst_n(rst_n), 
		.data(data)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst_n = 1;

		// Wait 100 ns for global reset to finish
		#100;
		rst_n = 0;
		#20 rst_n = 1;

        
		// Add stimulus here

	end

	always #5 clk = ~clk;
      
endmodule

