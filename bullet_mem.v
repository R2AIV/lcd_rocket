`timescale 1ns / 1ps


module bullet_mem(    
    input [3:0] line_number,
	 output reg [15:0] spr_data
    );
	 
initial	
begin
	spr_data <= 16'd0;
end

always @(*)
begin
	case(line_number)
		4'h0: spr_data <= 16'b0000000000000000;
		4'h1: spr_data <= 16'b0000000000000000;
		4'h2: spr_data <= 16'b0000000000000000;
		4'h3: spr_data <= 16'b0000000000000000;
		4'h4: spr_data <= 16'b0000000000000000;
		4'h5: spr_data <= 16'b0011111111111000;
		4'h6: spr_data <= 16'b0001111111111100;
		4'h7: spr_data <= 16'b0000111111111110;
		4'h8: spr_data <= 16'b0000111111111110;
		4'h9: spr_data <= 16'b0001111111111100;
		4'hA: spr_data <= 16'b0011111111111000;
		4'hB: spr_data <= 16'b0000000000000000;
		4'hC: spr_data <= 16'b0000000000000000;
		4'hD: spr_data <= 16'b0000000000000000;
		4'hE: spr_data <= 16'b0000000000000000;
		4'hF: spr_data <= 16'b0000000000000000;
		
		default: spr_data <= 16'b0000_0000_0000_0000;	
	endcase
end


endmodule

