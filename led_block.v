`timescale 1ns / 1ps

module led_block(
    input clk,
    output reg [7:0] led
    );

    reg [3:0] state_cntr;
    reg       state_chng;
    reg [31:0] freq_div;

initial
begin
	led 	   <= 8'b0;
	state_cntr <= 4'b0;	
	state_chng <= 1'b0;
	freq_div   <= 32'b0;
end

always @(posedge clk)
begin
	if(freq_div == 32'd25000000)
	begin
		freq_div <= 32'd0;
		state_chng <= ~state_chng;
	end
	else
	begin
		freq_div <= freq_div + 32'd1;
	end
end

always @(posedge state_chng)
begin
	if(state_cntr == 4'd8)
	begin
		state_cntr <= 4'd0;
	end

	else
	begin
		state_cntr <= state_cntr + 1;
	end

	case(state_cntr)
		4'd0: led <= 8'b10000001;
		4'd1: led <= 8'b01000010;
		4'd2: led <= 8'b00100100;
		4'd3: led <= 8'b00011000;
		4'd4: led <= 8'b00100100;
		4'd5: led <= 8'b01000010;
		4'd6: led <= 8'b10000001;
		4'd7: led <= 8'b00000000;
		default: led <= 8'b0;
	endcase
end

endmodule
