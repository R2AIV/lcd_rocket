`timescale 1ns / 1ps


module digit_tb;

	// Inputs
	reg [7:0] line_number;
	reg [3:0] digit;

	// Outputs
	wire [15:0] spr_data;

	// Instantiate the Unit Under Test (UUT)
	digit_signgen uut (
		.line_number(line_number), 
		.digit(digit), 
		.spr_data(spr_data)
	);

	initial begin
		// Initialize Inputs
		line_number = 0;
		digit = 0;

		// Wait 100 ns for global reset to finish
		#100;

		#20 line_number = 8'h13;
        
		// Add stimulus here

	end
      
endmodule

