`timescale 1ns / 1ps

module lcd_test(
	 input clk_50M,                 //FPGA???????
	 input reset_n,                 
	 input key1,                    //??1??LCD????
	 input key2,
	 input key3,
	 
 
	 output lcd_dclk,
	 output [7:0] lcd_r,
    	 output [7:0] lcd_g,
    	 output [7:0] lcd_b,
    	 output lcd_hsync,
    	 output lcd_vsync,
    	 output lcd_de,
	 output [7:0] led
     );


//-----------------------------------------------------------//
// ?????????480*272 60Hz LCD
//-----------------------------------------------------------//
parameter LinePeriod =525;           //????
parameter H_SyncPulse=41;            //?????(Sync a)
parameter H_BackPorch=2;             //????(Back porch b)
parameter H_ActivePix=480;           //?????(Display interval c)
parameter H_FrontPorch=2;            //????(Front porch d)
parameter Hde_start=43;
parameter Hde_end=523;

//-----------------------------------------------------------//
// ?????????480*272 60Hz LCD
//-----------------------------------------------------------//
parameter FramePeriod =286;           //????
parameter V_SyncPulse=10;             //?????(Sync o)
parameter V_BackPorch=2;              //????(Back porch p)
parameter V_ActivePix=272;            //?????(Display interval q)
parameter V_FrontPorch=2;             //????(Front porch r)
parameter Vde_start=12;
parameter Vde_end=284;


parameter RaketaPosX = 30;

  reg[10 : 0] x_cnt;
  reg[9 : 0]  y_cnt;
  reg[7 : 0] grid_data_1;
  reg[7 : 0] grid_data_2;
  reg[23 : 0] bar_data;
  reg[3 : 0] lcd_dis_mode;
  reg[7 : 0]  lcd_r_reg;
  reg[7 : 0]  lcd_g_reg;
  reg[7 : 0]  lcd_b_reg; 

  wire [10:0] RealX;
  wire [ 9:0] RealY;

  reg [18:0] MoveDivCntr;
  reg MoveClk;

  reg [8:0] XPos;
  reg [8:0] YPos;

  reg [8:0] EnemyX;
  reg [8:0] EnemyY;

  reg [8:0] BulletX;

  reg BulletMoveEnabled;
  reg EnemyIsAlive;

  reg [7:0] PlayerScore;
  reg [7:0] EnemyScore;

  wire [3:0] PlayerOnes;
  wire [3:0] PlayerTens;
  wire [3:0] PlayerHundreds;

  wire [3:0] EnemyOnes;
  wire [3:0] EnemyTens;
  wire [3:0] EnemyHundreds;

  initial
  begin
	  EnemyX = 460;
	  EnemyY = 130;
	  BulletX = 50;
	  XPos = 30;
	  YPos = 130;
	  BulletMoveEnabled = 1'b0;
	  EnemyIsAlive = 1'b1;
	  PlayerScore = 8'd0;
  end

  reg hsync_r;
  reg vsync_r; 
  reg hsync_de;
  reg vsync_de;
  
  reg [15:0] key1_counter;                 //???????
  
  wire lcd_clk;
  
  wire [12:0]  bar_interval;

 //LCD??????
  assign lcd_dclk = ~lcd_clk;
  assign lcd_hsync = hsync_r;
  assign lcd_vsync = vsync_r;
  assign lcd_de = hsync_de & vsync_de;
  assign lcd_r = (hsync_de & vsync_de)?lcd_r_reg:8'b00000000;
  assign lcd_g = (hsync_de & vsync_de)?lcd_g_reg:8'b00000000;
  assign lcd_b = (hsync_de & vsync_de)?lcd_b_reg:8'b00000000;
 
  assign	bar_interval 	= H_ActivePix[15: 3];         //????=H_ActivePix/8

//----------------------------------------------------------------
////////// ??????
//----------------------------------------------------------------
always @ (posedge lcd_clk)
       if(1'b0)    x_cnt <= 1;
       else if(x_cnt == LinePeriod) x_cnt <= 1;
       else x_cnt <= x_cnt+ 1;
		 
//----------------------------------------------------------------
////////// ??????hsync,hsync_de??
//----------------------------------------------------------------
always @ (posedge lcd_clk)
   begin
       if(~reset_n) hsync_r <= 1'b1;
       else if(x_cnt == 1) hsync_r <= 1'b0;            //??hsync??
       else if(x_cnt == H_SyncPulse) hsync_r <= 1'b1;
		 
		 		 
	    if(1'b0) hsync_de <= 1'b0;
       else if(x_cnt == Hde_start) hsync_de <= 1'b1;    //??hsync_de??
       else if(x_cnt == Hde_end) hsync_de <= 1'b0;	
	end

//----------------------------------------------------------------
////////// ??????
//----------------------------------------------------------------
always @ (posedge lcd_clk)
       if(~reset_n) y_cnt <= 1;
       else if(y_cnt == FramePeriod) y_cnt <= 1;
       else if(x_cnt == LinePeriod) y_cnt <= y_cnt+1;

//----------------------------------------------------------------
////////// ??????vsync, vsync_de??
//----------------------------------------------------------------
always @ (posedge lcd_clk)
  begin
       if(~reset_n) vsync_r <= 1'b1;
       else if(y_cnt == 1) vsync_r <= 1'b0;    //??vsync??
       else if(y_cnt == V_SyncPulse) vsync_r <= 1'b1;
		 
	    if(~reset_n) vsync_de <= 1'b0;
       else if(y_cnt == Vde_start) vsync_de <= 1'b1;    //??vsync_de??
       else if(y_cnt == Vde_end) vsync_de <= 1'b0;	 
  end

wor spr_r;
wor spr_g;
wor spr_b;

wire [8:0] RndEnemyPos;
assign RndEnemyPos[8] = 1'b0;
		
sprite_16x16 raketa (
    .PosX(RaketaPosX), 
    .PosY(YPos), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(spr_r), 
    .g(spr_g), 
    .b(spr_b)
    );

enemy_16x16 enemy (
    .PosX(EnemyX), 
    .PosY(EnemyY), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(spr_r), 
    .g(), 
    .b(spr_b)
    );

bullet_16x16 Bullet (
    .PosX(BulletX), 
    .PosY(YPos), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(spr_r), 
    .g(), 
    .b()
    );


rng8bit random_1 (
    .clk(MoveClk), 
    .rst_n(1'b1), 
    .data(RndEnemyPos[7:0])
    );

// ������� ���������������

signgen_16x16 P_Hundreds (
    .PosX(108), 
    .PosY(5), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .Digit(PlayerHundreds), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(spr_r), 
    .g(spr_g), 
    .b()
    );

signgen_16x16 P_Tens (
    .PosX(124), 
    .PosY(5), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .Digit(PlayerTens), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(spr_r), 
    .g(spr_g), 
    .b()
    );

signgen_16x16 P_Ones (
    .PosX(140), 
    .PosY(5), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .Digit(PlayerOnes), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(spr_r), 
    .g(spr_g), 
    .b()
    );

signgen_16x16 E_Hundreds (
    .PosX(408), 
    .PosY(5), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .Digit(EnemyHundreds), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(), 
    .g(), 
    .b(spr_b)
    );

signgen_16x16 E_Tens (
    .PosX(424), 
    .PosY(5), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .Digit(EnemyTens), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(), 
    .g(), 
    .b(spr_b)
    );

signgen_16x16 E_Ones (
    .PosX(440), 
    .PosY(5), 
    .CurrentX(RealX), 
    .CurrentY(RealY), 
    .Digit(EnemyOnes), 
    .clk50(lcd_clk), 
    .active(lcd_de), 
    .r(), 
    .g(), 
    .b(spr_b)
    );


bcd bcd_player_score (
    .number(PlayerScore), 
    .hundreds(PlayerHundreds), 
    .tens(PlayerTens), 
    .ones(PlayerOnes)
    );

bcd bcd_enemy_score (
    .number(EnemyScore), 
    .hundreds(EnemyHundreds), 
    .tens(EnemyTens), 
    .ones(EnemyOnes)
    );



assign RealX = x_cnt - 43;
assign RealY = y_cnt - 12;


// "�������� VGA �������. ���������� ��� � ��� ������������ ������������
// �������
wire out_r;
wire out_g;
wire out_b;

wire BorderR;

assign out_r = spr_r | BorderR;
assign out_g = spr_g;
assign out_b = spr_b;

assign BorderR = (RealX == 60);

always @(posedge lcd_clk)
begin
	lcd_r_reg[7] <= out_r;
	lcd_g_reg[7] <= out_g;
	lcd_b_reg[7] <= out_b;

	if(RealX[3:0] == 5 || RealY[3:0] == 5)
	begin
		lcd_r_reg[6:0] <= 7'd30;
		lcd_g_reg[6:0] <= 7'd10;
		lcd_b_reg[6:0] <= 7'd10;
	end
	else
	begin
		lcd_r_reg[6:0] <= 7'd0;
		lcd_g_reg[6:0] <= 7'd0;
		lcd_b_reg[6:0] <= 7'd0;
	end
end

always @(posedge MoveClk)
begin
	if(~key1) YPos <= YPos + 9'd1;
	if(~key3) YPos <= YPos - 9'd1;

	if(YPos > 250) YPos <= 9'd250;
	if(YPos < 20) YPos <= 9'd20;

	if(EnemyIsAlive)
	begin
		if(EnemyX == 65) 
			begin
				EnemyX <= 460;
				EnemyY <= RndEnemyPos;
				EnemyScore <= EnemyScore + 8'd1;				
			end
		else EnemyX <= EnemyX - 1;
	end
	else
	begin
		EnemyX <= 460;
	end

	if(~key2) BulletMoveEnabled <= 1'b1;

	if(BulletX == 460) BulletMoveEnabled <= 1'b0;

	if(BulletMoveEnabled)
	begin
		if(BulletX == 460) BulletX <= 50;
		else BulletX <= BulletX + 1;
	end


	// ��������� ������� �� ������� �� �����
	if(BulletMoveEnabled && 
		EnemyIsAlive && 
		(BulletX[8:3] == EnemyX[8:3]) && 
		(YPos[8:3] == EnemyY[8:3]))
	begin
		BulletMoveEnabled <= 1'b0;
		EnemyIsAlive <= 1'b0;
		EnemyX <= 460;
		
		// ��� ���������� ����� ��������� ����
		EnemyX <= 460;
		//XPos <= 30;
		//YPos <= 130;
		BulletX <= 50;
		BulletMoveEnabled <= 1'b0;
		EnemyIsAlive <= 1'b1;	
		EnemyY <= RndEnemyPos;
		PlayerScore <= PlayerScore + 8'd1;
	end
	else
	begin
		EnemyIsAlive <= 1'b1;
//		BulletMoveEnabled <= 1'b1;
	end

end

always @(posedge lcd_clk)
begin
	if(MoveDivCntr == 19'd40000) 
		begin
			MoveDivCntr <= 19'd0;
			MoveClk <= ~MoveClk;
		end
	else MoveDivCntr <= MoveDivCntr + 19'd1;	
end


pll pll_inst (
    .CLKIN_IN(clk_50M), 
    .RST_IN(~reset_n), 
    .CLKFX_OUT(lcd_clk), 
    .CLKIN_IBUFG_OUT(), 
    .CLK0_OUT(), 
    .LOCKED_OUT(LOCKED)
    );

led_block leds (
    .clk(clk_50M), 
    .led(led)
    );

endmodule

