`timescale 1ns / 1ps


module signgen_16x16(
    input [11:0] PosX,
    input [11:0] PosY,
    input [11:0] CurrentX,
    input [11:0] CurrentY,
    input [3:0] Digit,
    input clk50,
    input active,
    output r,
    output g,
    output b
    );

wire [15:0] spr_data;	// line sprite data
reg video_out;
reg [3:0] x_offset; 		// current pixel X position
reg [3:0] y_offset;

assign r = video_out;
assign g = video_out;
assign b = video_out;

initial
begin	
	x_offset <= 4'd0;
	y_offset <= 4'd0;
end

digit_signgen signgen (
    .line_number({Digit[3:0],y_offset[3:0]}), 
    .spr_data(spr_data)
    );

wire pixel_in_sprite = ((CurrentX >= PosX) && (CurrentX <= PosX + 15)) && ((CurrentY >= PosY) && (CurrentY <= PosY + 15));

always @(*)
begin
	// Count in-sprite coordinates if current (x,y) is in sprinte boundaries
	if((CurrentX >= PosX) && (CurrentX <= PosX + 15)) x_offset <= CurrentX - PosX; // not mirroring
	if((CurrentY >= PosY) && (CurrentY <= PosY + 15)) y_offset <= CurrentY - PosY;
end

always@(posedge clk50)
	video_out <= ((spr_data >> 15-x_offset) & 1'b1) && pixel_in_sprite;
	

endmodule

