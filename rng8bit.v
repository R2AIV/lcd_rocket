module rng8bit(
  input clk,
  input rst_n,
  output reg [7:0] data
);

reg [7:0] data_next;

initial
begin
	data <= 8'h1FF;
	data_next <= 8'h000;
end

always @* begin
	data_next[7] = data[5]^data[4];
	data_next[6] = data[5]^data[3];
	data_next[5] = data[5]^data[2];
  	data_next[4] = data[4]^data[1];
  	data_next[3] = data[3]^data[0];
  	data_next[2] = data[2]^data_next[4];
  	data_next[1] = data[1]^data_next[3];
  	data_next[0] = data[0]^data_next[2];
end

always @(posedge clk or negedge rst_n)
  if(!rst_n)
    data <= 8'h1FF;
  else
    data <= data_next;
endmodule

